<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Route::post('generate-token', 'MessagesController@generateToken');
Route::post('getChannel', 'MessagesController@getChannel');

Route::post('createChannel', 'MessagesController@createChannel');
Route::get('getAllChannels', 'MessagesController@getAllChannels');

Route::get('/test-channel',  'MessagesController@test');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
