<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use GetStream\StreamChat\Client;
use App\Message;
use App\Channel;

class MessagesController extends Controller
{
    protected $client;

    public function __construct()
    {
        $this->middleware('auth');
        $this->client =  new Client(
            getenv("STREAM_API_KEY"),
            getenv("STREAM_API_SECRET")
        );
    }

    public function generateToken(Request $request)
    {
        return response()->json([
            'token' => $this->client->createToken($request->input('name'))
        ], 200);
    }

  public function getAllChannels(){
     $channels = Channel::all();
     return response()->json([
            'channels' => $channels
     ], 200);
  }

   public function getChannel(Request $request){
        // $from = $request->input('from');
        // $to = $request->input('to');

        // $from_username = $request->input('from_username');
        // $to_username = $request->input('to_username');
        $from = "admin";
        $to = "client";

        $from_username = "admin";
        $to_username = "client";

        $channel_name = "livechat-{$from_username}-{$to_username}";

        $channel = $this->client->getChannel("messaging", $channel_name);
        $channel->create($from_username, [$to_username]);

        return response()->json([
            'channel' => $channel_name
        ], 200);
   }

   public function createChannel(Request $request){
        // $from = $request->input('from');
        // $to = $request->input('to');

        // $from_username = $request->input('from_username');
        // $to_username = $request->input('to_username');

        Channel::create([
          "name" => $request->name,
          "creator_id" => Auth::id(),
          "participant_id" => Auth::id(),
        ]);

        return response()->json([
            'created successfully'
        ], 200);
        // $from = $request->name;
        // $to = "client";

        // $from_username = "admin";
        // $to_username = "client";

        // $channel_name = "groupchat-{$from_username}-{$to_username}";

        // $channel = $this->client->getChannel("messaging", $channel_name);
        // $channel->create($from_username, [$to_username]);

        return response()->json([
            'channel' => $channel_name
        ], 200);
   }


}
